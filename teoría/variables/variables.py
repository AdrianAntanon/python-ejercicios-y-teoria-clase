#Las variables en Python no poseen un tipo definido de manera predeterminada
# y una misma variable puede contener en diferentes estados de ejecución de un programa diferentes tipos de datos
#Por ejemplo:
#una variable de tipo entero representa una referencia vinculada a una dirección de memoria donde se encuentra el valor 1 de un objeto de tipo entero.

variable = 10
print(variable)

#Podemos hacer una revinculación
#donde una variable con una determinada referencia es revinculada para que ahora contenga una referencia que apunte a otro dato.

variable = 'Hola mundo!'
print(variable)

# Cuando un objeto dejar de estar referenciado por alguna variable, este es eventualmente eliminado gracias a un mecanismo que poseen algunos lenguajes
# y que se relaciona con la administración automática de la memoria
# y que es conocido como el recolector de basura ( garbage collector )
#Variables globales y variables locales
# Una variable se dice que es global cuando está definida a nivel del script de Python y local cuando se han definido dentro de una función:

variable_global = 'Soy una variable global'
print('La variable global se puede imprimir desde todos lados, es GLOBAL -->' + variable_global)

def ejemplo ():
    variable_local = 'Soy una variable local'
    print('Una variable local solo existe dentro de la función o método en que lo creemos-->'+variable_local)

ejemplo()

#Variables de entorno
# se encuentran fuera del sistema de Python y se acceden por línea de pedido o Shell del sistema operativo.
# Se utilizan para configurar diferentes aspectos que son necesarios en el momento de ejecutar programas del lenguaje.
# Uno de estos aspectos es el camino físico que utilizarán los programas para acceder a los diferentes módulos de la instalación de Python;
# el valor de la variable PYTHONPATH o PATH se define con este propósito, la importación de los módulos.

# Exemple:
#
# import os
# for key in os.environ.keys():
# print (key)
# Per conèixer el valor de la variable PATH s'accedeix mitjançant os.environ:
#
# print (os.environ['PATH'])
# /home/sjo/PycharmProjects/prova/venv/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin



