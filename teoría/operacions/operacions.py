# Tipus numerics
print(int(2.55), float('1'), int('100', 5))

# Seqüencies
lista = ['hola', 1, 1.50]
print(lista + lista + lista)
print(lista * 3)
lista = 'Hola ' + 'mundo'
print(lista)

print('M' in 'Mundo')
print('M' in 'Hola')

lista = 'Hola mundo'
print(lista[-5] + lista[-4] + lista[-3] + lista[-2] + lista[-1])
print(lista[0:4])
print(lista[::-1])
num = [1, 2, 3, 4, 5]
num[1:3] = ['a', 'b']
print(num)

del num[1:3]
print(num)


#Diccionaris
print(len({1:'autor'}))

diccionari = {1:'hola', 2:'mundo'}
print(diccionari[2])

del diccionari[2]

print(diccionari)
