def imprimir(palabra):
    for i in palabra:
        print(i, end=' ' + '\n')
# Totes les dades són representats mitjançant objectes i cada objecte un tipus.
# El tipus d'un objecte determina les operacions i atributs que posseeix i si pot ser alterat.
# Un objecte que pot ser modificat es diu que és mutable i, en cas contrari, immutable.
# existeixen objectes predefinits que permeten la utilització de dades primitives:
# cadenes, números, «tuplas», llistes i diccionaris

# Una seqüència és un contenidor d'elements ordenats
# als quals és possible accedir mitjançant un índex que consisteix en un número sencer no negatiu (més gran o igual a zero).
# Ofereix tipus predefinits de seqüencies, cal destacar les cadenes, llistes i ''tuplas''

#Cadenes o String
# És un objecte immutable i qualsevol amb una cadena retorna una altra resultant de l'operació.
# Es pot definir una cadena tant utilitzant les cometes simples com les cometes dobles.

variable = 'Hola mundo'
print(variable)

# backslash para continuar en un altre linia
variable = 'Hola ' \
           'mundo'
print(variable)

#Llistes
# són seqüències d'elements ordenats que tenen la característica de ser mutables.
# Cada element pot ser de qualsevol tipus i la sintaxis per crear-la es bastant simple:

variable = ['Hola', 1, 1.99, 'mundo']
imprimir(variable)
#Una altra manera de crear llistes és mitjançant la funció list() que pren com argument una seqüència i retorna una llista amb cada element d'aquesta seqüència.
# En cas de no rebre cap argument retorna una llista buida.
print(list('Hola mundo'))

#Tuplas
# seqüència ordenada d'elements, que a diferència de la llista és immutable, això implica que quan la modifiquem generem una nova tupla, la inicial no es pot canviar.
# Els elements poden ser de qualsevol tipus.
variable = ('Perro', 0, 5.99, 'gato')
imprimir(variable)

#Diccionaris
# és una correspondència (de l'anglès mapping) que s'estableix en un multiconjunt de X elements

variable = {'Hola':1,'Mundo':2, 'Perro':133.23, 5:'gato'}
for i in variable:
    print(i,':',variable[i], end=' ')
print()

#Numerics
# disposa de tres objectes numèrics:
# els objectes que representen números sencers, números complexes i números en coma flotant.
enter = 1
complexe = .001e-3
coma_flotant = 1.11
print(enter, complexe,coma_flotant)

#None
# és l'equivalent a null de molts llenguatges de programació. No conté ni mètodes ni atributs.
# S'utilitza, entre altres coses, per crear una variable que el seu valor inicial no es coneix i serà conegut durant l'execució del programa.

#Booleans
#True o false
#exemple false y true
print(bool(0), bool(1))

#Conjunts
# són conjunts tradicionals, no multiconjunts de manera que només un exemplar de cada element repetit
# en la seqüència d'entrada és inclòs en el conjunt resultant.
variable = set([1,1,1,1,1,1,1,1,22,22,22,3,3,3,3,3,3])
imprimir(variable)