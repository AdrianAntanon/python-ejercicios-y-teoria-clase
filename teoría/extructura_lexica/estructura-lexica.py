#Identación correcta, en Python no hay delimitadores como {} o ; hay un sistema basado en espacios a la izquierda, lo conocemos como identacion.
lista = ["Hola","Mundo"]
#Ejemplo NO permitido
# for e in lista:
# print (e)
#Ejemplo permitido
for i in lista:
    print(i)


# Los tokens son elementos esenciales que se definen en la gramática de un lenguaje.
# En el proceso de compilación estos elementos son extraídos por un componente conocido como lexicográfico y entregados al analizador sintáctico.
# Entre estos elementos figuran los identificadores, las palabras reservadas, los operadores, los literales y los delimitadores.
# Existen otras partes del texto, como los comentarios, que en el Python van precedidos del carácter # y son ignorados por el compilador.

#Identificadores
# _variable --> Elemento privado
# __variable --> Elemento fuertemente privado
# __import__ --> Elemento especial definido del lenguaje

#Literales
#pueden ser enteros, decimales, cadenas, binarios, etc.
var1 = 1 #Integer
var1 = 1.234 #float
var1 = 'Hola' #String
var1 = "Hola" #String

#Delimitadores
#puede cumplir, entre otras, la función de servir de organizador de código
# ( ) [ ]
# { } , :
# . ' = ;
# + = - = * = / =
# // = % = & = | =
# ^ = >> = << = ** =
#Las tres últimas filas contienen los operadores de asignación incremental que no sólo sirven como delimitadores sino también realizan una determinada operación.

#Palabras reservadas
#son tokens que generalmente no pueden utilizarse como identificadores y se escriben en minúsculas. Ejemplo:
# and as assert break
# class continue def del
# Elif else except ejec
# finally for from global
# if import in is
# lambda 	not oro pass
# print raise return try
# while yield False None
# true

#Sentencias
#se puede descomponer en un conjunto de sentencias, que a su vez se pueden descomponer en simples o compuestas.