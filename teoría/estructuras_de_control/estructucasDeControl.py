#Ejemplos de estructuras de control

#Sentencia while
while True:
    try:
        respuesta = int(input('¿Cuántos años tienes?\n'))
        break
    except ValueError:
        print('Introduce un número entero, por favor')
#Sentencia if-elif-else
if respuesta > 18:
    print('Eres mayor de edad, espero que seas responsable')
elif respuesta == 18:
    print('Felicidades, oficialmente eres un adulto')
else:
    print('No tengas prisa en ser mayor de edad, disfruta el ser menor')
#Sentencia for
for i in 'Hola mundo':
    print(i, end='')