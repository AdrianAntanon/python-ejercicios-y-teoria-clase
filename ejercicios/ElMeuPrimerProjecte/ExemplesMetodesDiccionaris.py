# Adrian Antanyon
def imprimirDiccionario(diccionario):
    for i in diccionario:
        print(i, ':', diccionario[i], end=' ')
    print()
def imprimir(palabra):
    for i in palabra:
        print(i, end=' ')
    print()


MTDO = 'Método'
# Diccionaris
# és una correspondència (de l'anglès mapping) que s'estableix en un multiconjunt de X elements
print('Ejemplos de métodos en Diccionarios')
variable = {'Hola': 1, 'Mundo': 2, 'Animal': 133.23, 5: 'gato'}
imprimirDiccionario(variable)

#Has_key devuelve true si el diccionario tiene el objeto que mandamos por parámetro, actualmente no existe en PYTHON 3

#Items, devuelve una lista de los elementos del diccionario, haciendo tándem Clave-Valor ej: 'Num':1
print()
print(MTDO,'Items')
print(variable.items(),'\n')

#Keys, devuelve una lista con las llaves del diccionario
print(MTDO,'Keys')
print(variable.keys(),'\n')

#Values, lo mismo que el método keys pero solo con los valores
print(MTDO,'Values')
print(variable.values(),'\n')

#Iter, ya no existen las opciones itervalues,items o keys y son remplazadas por esta, lo que hace es como indica su nombre, iterar entre los valores que le pasemos por parámetro
diccIter = {1:100, 2:200, 3:300}
print(MTDO,'Iter')
imprimirDiccionario(diccIter)
llaves = iter(diccIter.keys())
print('Solo las llaves --> ',end='')
imprimir(llaves)
valores = iter(diccIter.values())
print('Solo los valores --> ',end='')
imprimir(valores)
print()

#Get, devuelve el valor correspondiente a la llave que le enviemos por parámetro, si no se existe devuelve NONE, podemos añadir otro parámetro para que si no existe nos devuelva eso en vez de NONE
print(MTDO,'Get')
print(diccIter.get(2))
print(diccIter.get(4,'NO EXISTE'),'\n')

#Clear, elimina todos los elementos del diccionario
print(MTDO,'Clear')
borrar = {10: 'Hola', 20:'Mundo'}
imprimirDiccionario(borrar)
print(borrar.clear(),'\n')

#Popitem, extrae el último llave-valor del diccionario
print(MTDO,'PopItem')
ejemploPopitem = {10: 'Hola', 20:'Mundo'}
imprimirDiccionario(ejemploPopitem)
print(ejemploPopitem.popitem())
imprimirDiccionario(ejemploPopitem)
print()

#Update, sería como un append para diccionarios, mejor ver el ejemplo
print(MTDO,'Update')
diccionariAntiguo = {1:10}
print('Sin realizar el update',end=' ')
imprimirDiccionario(diccionariAntiguo)
diccionariNuevo = {90:'Mundo'}
diccionariAntiguo.update(diccionariNuevo)
print('Realizado el update',end=' ')
imprimirDiccionario(diccionariAntiguo)
