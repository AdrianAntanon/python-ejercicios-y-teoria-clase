# Adrian Antanyon
def imprimir(palabra):
    for i in palabra:
        print(i, end=' ')
    print('\n')


#Llistes
# són seqüències d'elements ordenats que tenen la característica de ser mutables.
# Cada element pot ser de qualsevol tipus i la sintaxis per crear-la es bastant simple:
MTDO = 'Método '
listaEjemplo = ['Hola', 1, 1.99, 'mundo']
imprimir(listaEjemplo)
#Una altra manera de crear llistes és mitjançant la funció list() que pren com argument una seqüència i retorna una llista amb cada element d'aquesta seqüència.
# En cas de no rebre cap argument retorna una llista buida.
print(MTDO+'LIST')
print(list('Hola mundo'))
print()

#Append, sirve para añadir un valor al final de la lista
usoAppend = [1,2,3,'cuatro']
print(MTDO+'APPEND')
usoAppend.append('uso de Append')
imprimir(usoAppend)

#Insert, añadimos un valor donde le indiquemos dentro la lista, por lo que recibirá dos parámetros
print(MTDO+'INSERT')
listaEjemplo.insert(2,'-> ¡Esto es el ejemplo de INSERT! <-')
imprimir(listaEjemplo)

#Remove, elimina el primero objeto dentro una lista que coincida con el parámetro que le estamos pasando
print(MTDO+'REMOVE')
listaEjemplo.remove('-> ¡Esto es el ejemplo de INSERT! <-')
imprimir(listaEjemplo)

#Sort, ordena una lista usando la función comparer y en orden ascendente a no ser que le indiquemos otra cosa, si la lista es una mezcla de int y str NO FUNCIONA por defecto
usoSort = [4, 2, 5, 1, 0,100,-100]
print(MTDO+'SORT')
usoSort.sort()
imprimir(usoSort)

#Sorted, lo mismo que sort PERO solo la muestra ordenada, no la modifica, por lo que si queremos guardarla debe ser en otra variable.
lista_desordenada = [4,2,3,1]
usoSorted = sorted(lista_desordenada)
print(MTDO+'SORTED')
imprimir(usoSorted)

#Reverse, te muestra una lista del revés.
print(MTDO+'REVERSE')
usoSort.reverse()
imprimir(usoSort)

#Pop, elimina de la lista el elemento que le pasamos por parámetro y nos lo devuelve por si lo queremos guardar
usoPop = usoSort.pop(6)
print(MTDO+'POP')
print(usoPop, end=' <-- Se ha extraído de la lista UsoSort, ahora comprobamos si es verdad o no\n')
imprimir(usoSort)

#Extend, añade todos los elementos que pasemos por parámetro a una lista
usoExtend = [1000, 2000, 3000]
print(MTDO+'EXTEND')
print('Lista original',usoExtend,'\nLista original + usoSort ',end='')
usoExtend.extend(usoSort)
imprimir(usoExtend)

#Count, devuelve el número de veces que aparece dentro de la lista el objeto que le pasamos por parámetro
print(MTDO+'COUNT')
usoCount = [1, 1, 2, 3, 4, 1, 1, 1]
print('Veces que aparece el 1 dentro de nuestra lista ',usoCount, ' es = a ',usoCount.count(1),'\n')

#Index, nos devuelve la posición dentro de una lista donde aparece por primera vez el objeto que pasemos por parámetro
print(MTDO+'INDEX')
print('Vamos a usar la lista del ejemplo Extend que es ', usoExtend, '\ny quiero que me devuelva la posición donde aparece el 1, que es la', usoExtend.index(1))