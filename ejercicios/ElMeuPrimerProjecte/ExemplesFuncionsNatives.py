# Adrian Antanyon

FUNCION = 'Función '
def imprimirFuncion (funcionEspecifica):
    print(FUNCION + funcionEspecifica)


# Función abs(valor)
# Devuelve el valor absoluto de un número pasado por parámetro
# Si por lo que sea es un número complejo devuelve la raíz cuadrada de la suma de los cuadrados de la parte real y de la parte imaginaria

imprimirFuncion('abs(X)')
numero = -8.5
print(numero,'está en negativo, ahora usamos abs para calcular el valor absoluto y es ', abs(numero),'\n')


# Función all(valores)
# Devuelve true si todos los valores iterables son verdaderos, si uno de ellos no lo es devuelve falso por lo tanto.

imprimirFuncion('all(TRUE, FALSE)')
listaTrue =[1, 2, 3]
listaFalse=[1, 2, 3, 0]
print('Tengo la siguiente lista de elementos',listaTrue, 'si uso all nos dará', all(listaTrue), 'sin embargo con solo añadir un 0, pasa a ser', all(listaFalse),'\n')


# Función any(valores)
# Devuelve true si todos los valores están llenos y false si hay alguno vacío

imprimirFuncion('any([i, j, x])')
print(any([]),'está vacía y',any([1,2]), 'está llena','\n')


# Función apply(funcion,arg=(),keyword={})
# Llama a una función que pasamos por parámetro y nos devuelve el resultado de su ejecución

# No he encontrado forma de aplicarla en ningún ejemplo, y por todos los sitios que he buscado encontraba formas muy elaboradas y otras aplicacions


# Función bin(Integer)
# Devuelve una cadena que se corresponde con la representación binaria del entero que pasamos por parámetro
imprimirFuncion('bin(Integer)')
print(bin(3),'\n')


# Función bool(variable)
# Nos devuelve un 0 si es falso y un 1 si es verdadero
imprimirFuncion('bool(variable)')
print(bool(1),' ',bool(0),'\n')


# Función chr(Integer)
# Cadena de longitud 1 que representa el carácter correspondiente al código ASCII del parámetro
imprimirFuncion('chr(Integer)')
print(chr(6454),'<-- ejemplo\n')


# Función cmp(x,y)
# Nos devuelve un 0 sy x e y son iguales, si x<y un -1 y, por último, un 1 si x>y
imprimirFuncion('cmp(x,y)')
print('Función eliminada en Python 3','\n')


# Función compile(source, filename, mode)
# Source puede ser una cadena o AST y devuelve un obj de código ejecutable
# lanza el SyntaxError cuando source no es sintácticamente correcta. Y Mode puede ser
# ejecutado si source es un conjunto de sentencias o equivale si solo es una.

# No se me ha ocurrido nada simple como para poner un ejemplo


# Función delattr(object, name)
# Elimina un atributo del objeto indicándole el nombre del atributo en cuestión

# Tampoco se me ha ocurrido ningún ejemplo rápido y sencillo


# Función divmod(a,b)
# Recibe dos números naturales y devuelve una pareja que son cociente y resto de la división entera
imprimirFuncion('divmod(a,b)')
print('divmod(8, 3) =', divmod(8, 3),'\n')


# Función enumerate(x,start=0)
# Devuelve una tupla que comienza por el start que hemos indicado
imprimirFuncion('enumerate(iterable,start=0)')
listaDivmod = ['Java','Python','JS','SQL']
print(list(enumerate(listaDivmod,10)),'\n')


# Función eval(string)
# Analiza sintácticamente y evalua la expresión enviada por parámetro
imprimirFuncion('eval(string)')
x = 10
y = 5
print('eval("x+y") =',eval('x+y'),'\n')


# Función exect(object)
# Obligatorio que sea un String o un objeto de código, si es lo primero se analiza sintácticamente y se ejecuta, si es lo segundo se ejecuta y ya está.
imprimirFuncion('exect(object)')
program ='a = 5\nb=10\nprint("Sum =", a+b)'
print('Si analizamos esto',program,'nos da esto')
exec(program)
print('primero comprueba que sintácticamente esté correcto y luego lo ejecuta\n')


# Función filter(funcion, iterable)
# Construye un iterador donde sus elementos nos devuelven true si se cumple
imprimirFuncion('filter(f,iterable)')
abecedario = ['a','b','c','d','e']
print('Tenemos una lista con las primeras letras del abecedario',abecedario)
def filtrarVocales(abecedario):
    vocales = ['a', 'e', 'i', 'o', 'u']

    if(abecedario in vocales):
        return True
    else:
        return False

filtrarVocales = filter(filtrarVocales, abecedario)

print('Si filtramos las vocales usando filter nos daría:',end=' ')
for vocal in filtrarVocales:
    print(vocal,end=' ')

print('\n')


# Función getattr(object, name, valor por defecto)
# Devuelve el valor del atributo name del objeto pasado por parámetro, obligatorio que sea un String
imprimirFuncion('getattr(object,name)')
class Rectangulo:
    def __init__(self, b, h):
        self.b = b
        self.h = h

rect = Rectangulo(10, 5)
print("Area:", getattr(rect, "area",50),'\n')


#  Función hasattr(object, name)
#  toma como argumentos un objeto y el nombre de un atributo y retorna True si el objeto contiene dicho atributo.
imprimirFuncion('hasattr(object, name)')
print('Ejemplo de true hasattr(rect, "b") -->',hasattr(rect, "b"))
print('Ejemplo de false hasattr(rect, "Hola mundo") -->',hasattr(rect, "Hola mundo"),'\n')


# Función max(iterable)
# Devuelve el elemento máximo iterable no vacio
imprimirFuncion('max(iterable)')
listaNumeros = [5, 4, 3, 2, 1]
print(listaNumeros)
print('Ejemplo de max(listaNumeros)-->',max(listaNumeros),'\n')


# Función min(iterable)
# Devuelve el elemento mínimo iterable no vacío
imprimirFuncion('min(iterable)')
print(listaNumeros)
print('Ejemplo de min(listaNumeros)-->',min(listaNumeros),'\n')


# Función open(file)
# Abre un archivo indicándole la ruta al mismo
# un ejemplo sería ruta = C:/Users/Usuario/Downloads/ejemplo.txt -- print(open(ruta))


# Función pow(a,b)
# Devuelve A elevado a la potencia B, obligatorio que ambos sean Integer
imprimirFuncion('pow(a,b)')
print('Ejemplo pow(10,3) -->',pow(10,3),'\n')


# Función reversed(elementos)
# Devuelve un iterador con el orden invertido
imprimirFuncion('reversed(elements)')
listaReversed = [1,2,3,4]
print('Lista normal -->',listaReversed)
print('Lista usando reversed(listaReversed) -->',list(reversed(listaReversed)),'\n')


# Función round(x)
# Redondea al número X entero más cercano
imprimirFuncion('round(x)')
print('Tenemos el 4.6 que es -->',round(4.6))
print('Y con el 4.4 es -->',round(4.4),'\n')


# Función setattr(object, name, value)
# Asigna un valor al objeto name del objeto que hemos introducido por parámetro
imprimirFuncion('setattr(object,name,value)')
class Persona:
    nombre = 'Hola'
ejemplo = Persona()
print('Antes de setattr:', ejemplo.nombre)
setattr(ejemplo, 'nombre', 'Mundo')
print('Después de setattr:', ejemplo.nombre,'\n')


# Función str (objeto)
# Devuelve una representación como cadena del objeto pasado por parámetro
imprimirFuncion('str(object)')
hola = 'Hola mundo'
print('Uso de str(hola) -->',str(hola),'\n')


# Función sum(iterable)
# Devuelve la suma de todos los valores iterables
imprimirFuncion('sum(iterable)')
print('Lista normal -->',listaReversed)
print('Usando la función sum(listaReversed) -->',sum(listaReversed),'\n')


# Función type(object)
# devuelve el tipo de objeto que es
imprimirFuncion('type(object)')
print('Comprobamos que',5,'es',type(5))
print('También podemos ver con otros como -->','SOY UN STRING','<-- que es',type('SOY UN STRING'))











