# Adrian Antanyon
def compararListas(lista1=[],lista2=[]):
    if lista1 == lista2:
        print(lista1,'es igual que',lista2)
    else:
        print(lista1, 'es diferente a', lista2)

def comprobarNum(numero):
    contador = 0
    if numero < 1:
        print('Número inválido')
    else:
        for i in range(2, numero):
            if numero % i == 0:
                print('El número',numero,'no es primo')
                contador+=1
                break
        if contador == 0:
                print('El número',numero,'es primo')

def listaMultiplos(numero):
    if numero == 0:
        print('El número no puede ser 0, vuelva a introducirlo, por favor')
    else:
        i = numero+1
        while True:
            if i%numero == 0:
                print(i,end=', ')
            i+=1
#Crear 4 funciones

# 1.- Funció que determina si dos llistes són iguals. Dos llistes són iguals si tenen igual longitud i els seus elements en cada índex també ho són.
listaNumeros = [1,2,3]
listaNumeros2 = [1,2,4]

compararListas(listaNumeros,listaNumeros2)


# 2.- Funció que rep una matriu d’enters i retorna una tupla amb la llitsa o vector de la suma de cada fila i un altre vector amb la suma de cada columna.

# No he sabido entender lo que pide el ejercicio, soy incapaz de devolver las cosas que me pide.

# 3.- Funció que determina si un número passat per paràmetre és primer.
while True:
    primo = input('Introduce un número para saber si es primo o no, por favor:\n')
    try:
        numeroPrimo = int(primo)
        break
    except ValueError:
        print(primo,'no es un número entero')

comprobarNum(numeroPrimo)


# 4.- Funció que donat un número x retorna una llista infinita amb tots els múltiples de x.
while True:
    numeroMulti = input('Introduce un número para ver todos sus multiplos, por favor:\n')
    try:
        multi = int(numeroMulti)
        break
    except ValueError:
        print(numeroMulti,'no es un número entero')

listaMultiplos(multi)